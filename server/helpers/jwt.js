const jwt = require('jwt-simple');
const moment = require('moment');
const secret = 'pragol2021Monithor';

// Esta función crea el token JWT utilizando el nombre de usuario
exports.createToken = function(username){
    const payload = {
        sub: username, // Utiliza el nombre de usuario como identificador
        iat: moment().unix(),
        exp: moment().add(1,'day').unix()
    }

    return jwt.encode(payload, secret);
}

