const jwt = require('jwt-simple');
const moment = require('moment');
const secret = 'pragol2021Monithor';

// Middleware de autorización
exports.auth = function(req, res, next) {
    // Verificar si se proporciona el encabezado de autorización
    if (!req.headers.authorization) {
        return res.status(403).send({ message: 'NoHeadersError' });
    }

    // Obtener el token del encabezado de autorización
    const token = req.headers.authorization.replace(/['"]+/g, '');

    // Dividir el token en segmentos
    const segments = token.split('.');

    // Verificar si el token tiene tres segmentos
    if (segments.length !== 3) {
        return res.status(403).send({ message: 'InvalidToken' });
    }

    try {
        // Decodificar el token para obtener el payload
        const payload = jwt.decode(token, secret);
        
        // Verificar si el token ha expirado
        if (payload.exp <= moment().unix()) {
            return res.status(403).send({ message: 'TokenExpired' });
        }

        // Si el token es válido, adjuntar el payload decodificado a la solicitud
        req.user = payload;

        // Pasar al siguiente middleware
        next();
    } catch (error) {
        // Capturar cualquier error durante la decodificación del token
        return res.status(403).send({ message: 'InvalidToken' });
    }
};
