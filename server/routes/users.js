let router = require('express').Router();

const service = require('../services/users');

router
    .post('/login', service.login)
    .post('/register', service.register)
    .get('/listUser', service.list)

;   




module.exports = router;