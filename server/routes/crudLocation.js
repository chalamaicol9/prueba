let router = require('express').Router();
const authorization = require('../middlewares/authenticate');

const service = require('../services/crudLocation');

router
    .get('/',authorization.auth, service.getLocationsAll)
    .get('/:id',authorization.auth, service.getLocationById)
    .post('/crear',authorization.auth, service.createLocation)
    .delete('/eliminar/:id',authorization.auth, service.deleteLocation)
    .put('/actualizar/:id',authorization.auth, service.updateLocation)
   
; 

module.exports = router;