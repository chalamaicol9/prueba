let router = require('express').Router();
const authorization = require('../middlewares/authenticate');

const service = require('../services/crudEpisode');

router.get('/',authorization.auth, service.getAllEpisodes);
router.get('/:id', authorization.auth,service.getEpisodeById);
router.post('/crear',authorization.auth, service.createEpisode);
router.delete('eliminar/:id',authorization.auth, service.deleteEpisode);
router.put('actualizar/:id',authorization.auth, service.updateEpisode);

module.exports = router;