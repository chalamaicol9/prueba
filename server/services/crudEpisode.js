const path   = require('path');
const sqlite = require('sqlite3');
const {open} = require('sqlite');
const jwt = require('../helpers/jwt');

const fetch  = require('node-fetch');
const { maxHeaderSize } = require('http');
const bcrypt = require("bcrypt-nodejs");

require('dotenv').config();

const apiBaseURL = process.env.API_BASE_URL || 'http://localhost'

async function createEpisode(req, res) {
    let dbname = process.env.CRUD_DBNAME || 'data';
    try {
        const { name, air_date, episode, characters, url, created } = req.body;

        if (!name || !air_date || !episode || !characters || !url || !created) {
            return res.status(400).json({ error: 'faltan datos' });
        }

        const knex = require('knex')({
            client: 'sqlite3',
            connection: {
                filename: path.resolve('./data/rickandmorty_v1.db'),
            },
        });

        const [episodeId] = await knex('episodes').insert({ name, air_date, episode, characters, url, created });

        res.status(201).json({ id: episodeId, message: 'Episode creado' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'error' });
    }
}

async function getAllEpisodes(req, res) {
    let dbname = process.env.CRUD_DBNAME || 'data';
    try {
        const knex = require('knex')({
            client: 'sqlite3',
            connection: {
                filename: path.resolve(`./data/${dbname}.db`),
            },
        });

        const episodes = await knex('episodes').select('*');

        res.status(200).json(episodes);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: ' error' });
    }
}

async function getEpisodeById(req, res) {
    let dbname = process.env.CRUD_DBNAME || 'data';
    try {
        const { id } = req.params;

        const knex = require('knex')({
            client: 'sqlite3',
            connection: {
                filename: path.resolve(`./data/${dbname}.db`),
            },
        });

        const episode = await knex('episodes').where({ id }).first();

        if (!episode) {
            return res.status(404).json({ error: 'Episodio no encontrado' });
        }

        res.status(200).json(episode);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'error' });
    }
}

async function updateEpisode(req, res) {
    let dbname = process.env.CRUD_DBNAME || 'data';
    try {
        const { id } = req.params;
        const { name, air_date, episode, characters, url, created } = req.body;

        if (!name || !air_date || !episode || !characters || !url || !created) {
            return res.status(400).json({ error: 'faltan datos' });
        }

        const knex = require('knex')({
            client: 'sqlite3',
            connection: {
                filename: path.resolve(`./data/${dbname}.db`),
            },
        });

        const existingEpisode = await knex('episodes').where({ id }).first();

        if (!existingEpisode) {
            return res.status(404).json({ error: 'Episode no encontrado' });
        }

        await knex('episodes').where({ id }).update({ name, air_date, episode, characters, url, created });

        res.status(200).json({ message: 'Episode actualizado con exito' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'error' });
    }
}

async function deleteEpisode(req, res) {
    let dbname = process.env.CRUD_DBNAME || 'data';
    try {
        const { id } = req.params;

        const knex = require('knex')({
            client: 'sqlite3',
            connection: {
                filename: path.resolve(`./data/${dbname}.db`),
            },
        });

        const existingEpisode = await knex('episodes').where({ id }).first();

        if (!existingEpisode) {
            return res.status(404).json({ error: 'Episode no encontrado' });
        }

        await knex('episodes').where({ id }).del();

        res.status(200).json({ message: 'Episode eliminado con exito' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'error' });
    }
}

module.exports = {
    createEpisode,
    getAllEpisodes,
    getEpisodeById,
    updateEpisode,
    deleteEpisode
}