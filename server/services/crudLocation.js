const path   = require('path');
const sqlite = require('sqlite3');
const {open} = require('sqlite');
const jwt = require('../helpers/jwt');

const fetch  = require('node-fetch');
const { maxHeaderSize } = require('http');
const bcrypt = require("bcrypt-nodejs");

require('dotenv').config();

const apiBaseURL = process.env.API_BASE_URL || 'http://localhost'


async function getLocationsAll(req, res){
    let dbname = process.env.CRUD_DBNAME || 'data';
    let db_entity = 'locations';
    const knex = require('knex')({
        client: 'sqlite3',
        connection: {
            filename: path.resolve(`./data/${dbname}.db`),
        },
    });
    let respuesta= await knex(db_entity).select('*');
    res
    .status(200)
    .send(respuesta)
    .end()
;
}

async function getLocationById(req, res) {
    const locationId = req.params.id; // Obtener el ID de la ubicación desde los parámetros de la solicitud
    let dbname = process.env.CRUD_DBNAME || 'data';
    let db_entity = 'locations';
    const knex = require('knex')({
        client: 'sqlite3',
        connection: {
            filename: path.resolve(`./data/${dbname}.db`),
        },
    });
    try {
        const location = await knex(db_entity).where({ id: locationId }).first(); // Consulta para obtener la ubicación por su ID
        if (location) {
            res.status(200).json(location);
        } else {
            res.status(404).json({ error: 'Location no encontrado' });
        }
    } catch (error) {
        res.status(500).json({ error: 'error' });
    }
}

async function createLocation(req, res) {
    const { name, type, dimension, residents, url, created } = req.body; // Obtener los datos de la nueva ubicación desde el cuerpo de la solicitud
    let dbname = process.env.CRUD_DBNAME || 'data';
    let db_entity = 'locations';
    const knex = require('knex')({
        client: 'sqlite3',
        connection: {
            filename: path.resolve(`./data/${dbname}.db`),
        },
    });
    try {
        const newLocation = await knex(db_entity).insert({ name, type, dimension, residents, url, created }); // Insertar la nueva ubicación en la base de datos
        res.status(201).json({ id: newLocation[0] });
    } catch (error) {
        res.status(500).json({ error: 'error' });
    }
}

async function updateLocation(req, res) {
    try {
        const { id } = req.params; // Obtener el ID de la ubicación a actualizar desde los parámetros de la URL
        const { name, type, dimension, residents, url, created } = req.body;

      if (!name || !type || !dimension || !residents || !url || !created) {
            return res.status(400).json({ error: 'faltan campos' });
        }

        let dbname = process.env.CRUD_DBNAME || 'data';
        let db_entity = 'locations';

        const knex = require('knex')({
            client: 'sqlite3',
            connection: {
                filename: path.resolve(`./data/${dbname}.db`),
            },
        });

       const existingLocation = await knex(db_entity).where({ id }).first();
        if (!existingLocation) {
            return res.status(404).json({ error: 'Location no encontrado' });
        }

        
        await knex(db_entity).where({ id }).update({ name, type, dimension, residents, url, created });

        res.status(200).json({ message: 'Location actualizado' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'error' });
    }
}

async function deleteLocation(req, res) {
    const locationId = req.params.id; // Obtener el ID de la ubicación desde los parámetros de la solicitud
    let dbname = process.env.CRUD_DBNAME || 'data';
    let db_entity = 'locations';
    const knex = require('knex')({
        client: 'sqlite3',
        connection: {
            filename: path.resolve(`./data/${dbname}.db`),
        },
    });
    try {
        const deletedRows = await knex(db_entity).where({ id: locationId }).del(); // Eliminar la ubicación por su ID
        if (deletedRows > 0) {
            res.status(200).json({ message: 'Location eliminado correctamente' });
        } else {
            res.status(404).json({ error: 'Location not found' });
        }
    } catch (error) {
        res.status(500).json({ error: 'error' });
    }
}



module.exports = {
    getLocationsAll,
    getLocationById,
    createLocation,
    deleteLocation,
    updateLocation
}

