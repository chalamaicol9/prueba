const path   = require('path');
const sqlite = require('sqlite3');
const {open} = require('sqlite');
const jwt = require('../helpers/jwt');

const fetch  = require('node-fetch');
const { maxHeaderSize } = require('http');
const bcrypt = require("bcrypt-nodejs");

require('dotenv').config();

const apiBaseURL = process.env.API_BASE_URL || 'http://localhost'

async function login(req, res){
    const {
        username,
        password
    } = req.body;

    let dbname = process.env.CRUD_DBNAME || 'data';
    let db_entity = 'users';

    const knex = require('knex')({
        client: 'sqlite3',
        connection: {
            filename: path.resolve(`./data/${dbname}.db`),
        },
    });

    let sql = `SELECT * FROM ${db_entity} WHERE username = ?;`;
    let rs = await knex.raw(sql, [username]);

    if(rs.length === 0){
        res.status(401).send('401: not authorized').end();
    } else {
        let user = rs[0];
        let passwordHash = user.password;
        let passwordMatch = bcrypt.compareSync(password, passwordHash);

        if(passwordMatch){
            // Si la contraseña coincide, genera el token JWT utilizando el nombre de usuario
            const token = jwt.createToken(username);

            // Devuelve el token en la respuesta
            res.status(200).send({ token }).end();
        } else {
            res.status(401).send('401: not authorized').end();
        }
    }
}

async function register(req, res){
    const {
        username,
        password
    } = req.body;

    let dbname = process.env.CRUD_DBNAME || 'data';
    let db_entity = 'users';

    const knex = require('knex')({
        client: 'sqlite3',
        connection: {
            filename: path.resolve(`./data/${dbname}.db`),
        },
    });

    let passwordHash = bcrypt.hashSync(password);

    let sql = `INSERT INTO ${db_entity} (username, password) VALUES (?, ?);`;
    let rs = await knex.raw(sql, [username, passwordHash]);

    // Consultar la fila recién creada
    let insertedRow = await knex(db_entity)
        .where({ username: username })
        .first();

    res
        .status(200)
        .send({
            user: insertedRow, 
        })
        .end()
    ;
}
//listar los usuarios
async function list(req, res){
    let dbname = process.env.CRUD_DBNAME || 'data';
    let db_entity = 'users';

    const knex = require('knex')({
        client: 'sqlite3',
        connection: {
            filename: path.resolve(`./data/${dbname}.db`),
        },
    });

    let rs = await knex(db_entity).select('*');

    res
        .status(200)
        .send(rs)
        .end()
    ;
}

module.exports = {
    login,
    register,
    list
   
};
