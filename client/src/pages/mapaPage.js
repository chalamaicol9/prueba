import React, { useEffect, useState } from 'react';

function HeatmapPage() {
    const [heatmapData, setHeatmapData] = useState([]);

    useEffect(() => {
        loadEpisodes();
    }, []);

    async function loadEpisodes() {
        try {
            const response = await fetch('http://localhost:7000/episodes');
            const jsonResult = await response.json();
            const episodes = jsonResult.results;

            const updatedHeatmapData = episodes.map(episode => {
                // Calcular la cantidad de personajes por episodio
                const charactersCount = episode.characters.length;

                return {
                    season: episode.season,
                    episodeNumber: episode.episode,
                    charactersCount: charactersCount
                };
            });

            setHeatmapData(updatedHeatmapData);
        } catch (error) {
            console.error('Error loading episodes:', error);
        }
    }

    return (
        <div className="character-heatmap-page">
            <h1>Mapa de Calor de Personajes por Episodio</h1>
            <div className="heatmap">
                {heatmapData.map(data => (
                    <div key={`${data.season}-${data.episodeNumber}`} className="heatmap-cell">
                        <span>{`Temporada ${data.season}, Episodio ${data.episodeNumber}`}</span>
                        <div className="characters-count" style={{ backgroundColor: getColor(data.charactersCount) }}>
                            {data.charactersCount}
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}

function getColor(count) {
    // Definir colores basados en la cantidad de personajes
    // Por ejemplo, puedes usar diferentes tonos de rojo para representar diferentes cantidades
    // Puedes ajustar esta función según tus necesidades específicas
    return `rgb(${255 - count * 10}, 100, 100)`;
}

export default HeatmapPage;
