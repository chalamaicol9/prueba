Instrucciones de Uso
1.Clonar el Repositorio:

Para clonar este repositorio, ejecuta el siguiente comando en tu terminal:


git clone https://gitlab.com/chalamaicol9/prueba.git
Instalación de Dependencias:

2. Navega a las carpetas client y server por separado y ejecuta el siguiente comando para instalar las dependencias necesarias:


cd client
npm install

cd server
npm install


3. Autenticación por Token JWT:

Se ha añadido autenticación por token JWT para la seguridad de la aplicación. Los usuarios deben autenticarse antes de acceder a ciertas funcionalidades.

Se ha creado la entidad de usuarios para permitir el inicio de sesión en la aplicación. Aquí está el script SQL para crear la tabla users en SQLite:

sql
CREATE TABLE users (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT NOT NULL UNIQUE,
    password TEXT NOT NULL
);
4. Encriptación de Contraseña:

Las contraseñas de los usuarios se almacenan en la base de datos encriptadas para garantizar la seguridad de la información.

5. CRUD de Episodios y Lugares:

Se ha implementado un CRUD (Crear, Leer, Actualizar, Eliminar) para los episodios y lugares en el servidor. Puedes probar estas funcionalidades realizando solicitudes HTTP a las rutas correspondientes.

6. Vista para Personajes por Temporada y Episodio:

Se ha añadido una vista en el frontend que muestra la cantidad de personajes que aparecen en cada temporada y episodio en forma de mapa de calor.

7. Prueba de la Aplicación
Para probar la aplicación, sigue estos pasos:

Ejecuta el servidor ejecutando npm start dentro de la carpeta server.
Navega a http://localhost:7000 en tu navegador web.
Ejecuta el cliente ejecutando npm start dentro de la carpeta client.
Navega a http://localhost:3000 en tu navegador web.
¡Disfruta explorando la API de Rick & Morty con esta aplicación!